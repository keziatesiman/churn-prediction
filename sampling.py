import os, warnings, sys
import pandas as pd, numpy as np, time

from sklearn.model_selection import StratifiedShuffleSplit

#Fungsi Membuat strata/kategori pada user
def make_category(X) :
    if (X <= 50) :
        return 'A'
    elif (X>50 and X<=60) :
        return 'B' 
    elif (X>60 and X<=70) :
        return 'C' 
    elif (X>70 and X<=80) :
        return 'D' 
    elif (X>80 and X<=90) :
        return 'E' 
    else :
        return 'F' 


def pick_sample_random(df, num_of_voucher):
    df["numeric_prob"] = df["churn_proba"].apply(lambda x : (float(x[:-1]) ))
    df = df.sort_values(by=['numeric_prob'], ascending=False)
    df["prob_cat"] = df["numeric_prob"].apply(make_category)
    
    
    f_category= (df['prob_cat'] == 'F').sum()
    df2 = (df[df['prob_cat'] == 'F'])

    df_new = df.loc[df["prob_cat"] != 'A'].sort_values(by=['numeric_prob'], ascending=False)

    if (float(num_of_voucher) <= f_category):
        df_result = df_new.head(int(num_of_voucher))
        df_result = df_result.drop(['Unnamed: 0', 'prob_cat'], axis=1)
        df_result = df_result.sort_values(by=['numeric_prob'], ascending=False)

    else :
        df_new = df.loc[(df["prob_cat"] != 'F') & (df["prob_cat"] != 'A') ]
        df_new = df_new.reset_index()


        sizes = ((float(num_of_voucher)- f_category)/df_new.shape[0])

        if (sizes>=1): # diambil semua yg > 50%  dan yang < 50% diambil yg paling atas
            df = df.sort_values(by=['numeric_prob'], ascending=False)
            df_result = df.head(int(num_of_voucher))

        else: 

            split = StratifiedShuffleSplit(n_splits=1, test_size=sizes, random_state=42)
            for train_index, test_index in split.split(df_new, df_new["prob_cat"]):
                strat_train_set = df_new.loc[train_index]
                strat_test_set = df_new.loc[test_index] #Yang di save yang ini

            frames = [df2,strat_test_set]

            df_result = pd.concat(frames)
        df_result = df_result.drop(['Unnamed: 0', 'prob_cat'], axis=1)
        df_result = df_result.sort_values(by=['numeric_prob'], ascending=False)

    return df_result


def pick_sample_ordered(df, num_of_voucher):
    df["numeric_prob"] = df["churn_proba"].apply(lambda x : (float(x[:-1]) ))
    df = df.sort_values(by=['numeric_prob'], ascending=False)
    df["prob_cat"] = df["numeric_prob"].apply(make_category)
    df = df.sort_values(by=['numeric_prob'], ascending=False)
    df_result = df.head(int(num_of_voucher))
    df_result = df_result.drop(['Unnamed: 0', 'prob_cat'], axis=1)
    df_result = df_result.sort_values(by=['numeric_prob'], ascending=False)
    return df_result