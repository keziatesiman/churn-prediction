

def get_user_level() :
    X = """
    select "userId", "fullName","userLevel"
    from "pf-niffler"."public"."user"
    """
    return X

def get_sales(month,year,product) :
    
    X = ''' 
    select 
    user_id,  
    extract(month from date) as month, 
    extract(year from date) as year, 
    product_type, 
    sum(order_count) as num_of_order, 
    sum(sum_invoice_amount) as total_invoice, 
    sum(max_invoice_amount) as max_invoice, 
    sum(min_invoice_amount) as min_invoice, 
    avg(avg_invoice_amount) as avg_invoice, 
    sum(sum_product_price) as total_price, 
    sum(max_product_price) as max_price, 
    sum(min_product_price) as min_price, 
    avg(avg_product_price) as avg_price 
    from dwh.report.sales_daily 
    where "date" >= date_trunc('month', to_timestamp('{year}-{month}','YYYY-MM')) - interval '1 month' 
    and "date" < date_trunc('month', to_timestamp('{year}-{month}','YYYY-MM')) 
    and "product_type" = '{product}'
    group by 1,2,3,4 
    order by year desc 
    '''.format(
        year = year,
        month = month,
        product = product
    )
    return X