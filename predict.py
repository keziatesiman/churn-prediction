from sklearn.base import BaseEstimator, TransformerMixin
from data_generator import generate_dataframe
import pandas as pd, numpy as np, time
from sklearn.preprocessing import LabelBinarizer, LabelEncoder, StandardScaler

import pickle


class DataFrameSelector(BaseEstimator, TransformerMixin):
  def __init__(self, attribute_names):
    self.attribute_names = attribute_names
  def fit(self, X, y=None):
    return self
  def transform(self, X):
    return X[self.attribute_names].values

class MyLabelBinarizer(TransformerMixin):
    def __init__(self, *args, **kwargs):
        self.encoder = LabelBinarizer(*args, **kwargs)
    def fit(self, x, y=0):
        self.encoder.fit(x)
        return self
    def transform(self, x, y=0):
        return self.encoder.transform(x)

def to_percent(x) :
    return "{:.2%}".format(float(x))


def predict_churn(month,year,product) :

    #Generate the dataframe 

    df_fullName, df_user = generate_dataframe(month, year, product)

    #import model
    with open('rf_model2.pkl', 'rb') as f:
        model = pickle.load(f)

    #Create Prediction and append to dataframe
    probabilities = model.predict_proba(df_user)[:,1]
    data = {'churn_proba': probabilities}
    df_proba =pd.DataFrame(data=data)
    
    df_result = pd.concat([df_user, df_fullName, df_proba], axis=1)

    #Drop unnecessary columns
    
    df_result['churn_proba'] = pd.DataFrame(df_result['churn_proba'].apply(to_percent))
    unnecessary = ['month','year','product_type', 'num_of_order',
       'total_invoice', 'max_invoice', 'min_invoice', 'avg_invoice',
       'total_price', 'max_price', 'min_price', 'avg_price']
    df_result_min = df_result.drop(unnecessary, axis=1)
    return df_result, df_result_min 

    #Turn df to JSON
    # df_result.to_json("results/%s_%s_%s.json" % (month, year, product), orient='values')