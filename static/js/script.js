(function() {
  var app;

  app = angular.module('demo', []);

  app.directive('rmMonthSelect', function() {
    var i, j, months, years;
    years = [];
    for (i = j = 0; j <= 2; i = ++j) {
      years.push(1900 + (new Date).getYear() - i);
    }
    months = [
      {
        number: 1,
        text: 'January'
      },
      {
        number: 2,
        text: 'February'
      },
      {
        number: 3,
        text: 'March'
      },
      {
        number: 4,
        text: 'April'
      },
      {
        number: 5,
        text: 'May'
      },
      {
        number: 6,
        text: 'June'
      },
      {
        number: 7,
        text: 'July'
      },
      {
        number: 8,
        text: 'August'
      },
      {
        number: 9,
        text: 'September'
      },
      {
        number: 10,
        text: 'October'
      },
      {
        number: 11,
        text: 'November'
      },
      {
        number: 12,
        text: 'December'
      }
    ];
    return {
      restrict: 'E',
      scope: {
        rmModel: '='
      },
      template: "<span class=\"rm-select\"><select ng-model=\"rmModel.year\" ng-options=\"y for y in years\"></select></span><span class=\"rm-select\"><select ng-model=\"rmModel.month\" ng-options=\"n.number as n.text disable when invalidMonth(n.number) for n in months\"></select></span>",
      link: function(scope, el, attrs) {
        el.addClass('rm-month-select');
        scope.rmModel = {
          month: (new Date).getMonth() + 1,
          year: (new Date).getYear() + 1900
        };
        scope.months = months;
        scope.years = years;
        return scope.invalidMonth = function(m) {
          if (scope.rmModel.year !== (new Date).getYear() + 1900) {
            return false;
          }
          if (m > ((new Date).getMonth() + 1)) {
            return true;
          }
        };
      }
    };
  });

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiPGFub255bW91cz4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFBLEdBQUEsR0FBTSxPQUFPLENBQUMsTUFBUixDQUFlLE1BQWYsRUFBdUIsRUFBdkI7O0VBRU4sR0FBRyxDQUFDLFNBQUosQ0FBYyxlQUFkLEVBQStCLFFBQUEsQ0FBQSxDQUFBO0FBQzdCLFFBQUEsQ0FBQSxFQUFBLENBQUEsRUFBQSxNQUFBLEVBQUE7SUFBQSxLQUFBLEdBQVE7SUFFUixLQUFTLDBCQUFUO01BQ0UsS0FBSyxDQUFDLElBQU4sQ0FBVyxJQUFBLEdBQU8sQ0FBQyxJQUFJLElBQUwsQ0FBVSxDQUFDLE9BQVgsQ0FBQSxDQUFQLEdBQThCLENBQXpDO0lBREY7SUFHQSxNQUFBLEdBQVM7TUFDUDtRQUFBLE1BQUEsRUFBUSxDQUFSO1FBQ0EsSUFBQSxFQUFNO01BRE4sQ0FETztNQUlQO1FBQUEsTUFBQSxFQUFRLENBQVI7UUFDQSxJQUFBLEVBQU07TUFETixDQUpPO01BT1A7UUFBQSxNQUFBLEVBQVEsQ0FBUjtRQUNBLElBQUEsRUFBTTtNQUROLENBUE87TUFVUDtRQUFBLE1BQUEsRUFBUSxDQUFSO1FBQ0EsSUFBQSxFQUFNO01BRE4sQ0FWTztNQWFQO1FBQUEsTUFBQSxFQUFRLENBQVI7UUFDQSxJQUFBLEVBQU07TUFETixDQWJPO01BZ0JQO1FBQUEsTUFBQSxFQUFRLENBQVI7UUFDQSxJQUFBLEVBQU07TUFETixDQWhCTztNQW1CUDtRQUFBLE1BQUEsRUFBUSxDQUFSO1FBQ0EsSUFBQSxFQUFNO01BRE4sQ0FuQk87TUFzQlA7UUFBQSxNQUFBLEVBQVEsQ0FBUjtRQUNBLElBQUEsRUFBTTtNQUROLENBdEJPO01BeUJQO1FBQUEsTUFBQSxFQUFRLENBQVI7UUFDQSxJQUFBLEVBQU07TUFETixDQXpCTztNQTRCUDtRQUFBLE1BQUEsRUFBUSxFQUFSO1FBQ0EsSUFBQSxFQUFNO01BRE4sQ0E1Qk87TUErQlA7UUFBQSxNQUFBLEVBQVEsRUFBUjtRQUNBLElBQUEsRUFBTTtNQUROLENBL0JPO01Ba0NQO1FBQUEsTUFBQSxFQUFRLEVBQVI7UUFDQSxJQUFBLEVBQU07TUFETixDQWxDTzs7V0FxQ1Q7TUFDRSxRQUFBLEVBQVUsR0FEWjtNQUVFLEtBQUEsRUFDRTtRQUFBLE9BQUEsRUFBUztNQUFULENBSEo7TUFJRSxRQUFBLEVBQVUsaVJBSlo7TUFNRSxJQUFBLEVBQU0sUUFBQSxDQUFDLEtBQUQsRUFBUSxFQUFSLEVBQVksS0FBWixDQUFBO1FBQ0osRUFBRSxDQUFDLFFBQUgsQ0FBWSxpQkFBWjtRQUNBLEtBQUssQ0FBQyxPQUFOLEdBQ0U7VUFBQSxLQUFBLEVBQVEsQ0FBQyxJQUFJLElBQUwsQ0FBVSxDQUFDLFFBQVgsQ0FBQSxDQUFBLEdBQXdCLENBQWhDO1VBQ0EsSUFBQSxFQUFNLENBQUMsSUFBSSxJQUFMLENBQVUsQ0FBQyxPQUFYLENBQUEsQ0FBQSxHQUF1QjtRQUQ3QjtRQUVGLEtBQUssQ0FBQyxNQUFOLEdBQWU7UUFDZixLQUFLLENBQUMsS0FBTixHQUFjO2VBRWQsS0FBSyxDQUFDLFlBQU4sR0FBcUIsUUFBQSxDQUFDLENBQUQsQ0FBQTtVQUNuQixJQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBZCxLQUFzQixDQUFDLElBQUksSUFBTCxDQUFVLENBQUMsT0FBWCxDQUFBLENBQUEsR0FBdUIsSUFBaEQ7QUFDRSxtQkFBTyxNQURUOztVQUdBLElBQUcsQ0FBQSxHQUFJLENBQUMsQ0FBQyxJQUFJLElBQUwsQ0FBVSxDQUFDLFFBQVgsQ0FBQSxDQUFBLEdBQXdCLENBQXpCLENBQVA7QUFDRSxtQkFBTyxLQURUOztRQUptQjtNQVJqQjtJQU5SO0VBM0M2QixDQUEvQjtBQUZBIiwic291cmNlc0NvbnRlbnQiOlsiYXBwID0gYW5ndWxhci5tb2R1bGUgJ2RlbW8nLCBbXVxuXG5hcHAuZGlyZWN0aXZlICdybU1vbnRoU2VsZWN0JywgLT5cbiAgeWVhcnMgPSBbXVxuICBcbiAgZm9yIGkgaW4gWzAuLjJdXG4gICAgeWVhcnMucHVzaCgxOTAwICsgKG5ldyBEYXRlKS5nZXRZZWFyKCkgLSBpKVxuICAgIFxuICBtb250aHMgPSBbXG4gICAgbnVtYmVyOiAxXG4gICAgdGV4dDogJ0phbnVhcnknXG4gICxcbiAgICBudW1iZXI6IDJcbiAgICB0ZXh0OiAnRmVicnVhcnknXG4gICxcbiAgICBudW1iZXI6IDNcbiAgICB0ZXh0OiAnTWFyY2gnXG4gICxcbiAgICBudW1iZXI6IDRcbiAgICB0ZXh0OiAnQXByaWwnXG4gICxcbiAgICBudW1iZXI6IDVcbiAgICB0ZXh0OiAnTWF5J1xuICAsXG4gICAgbnVtYmVyOiA2XG4gICAgdGV4dDogJ0p1bmUnXG4gICxcbiAgICBudW1iZXI6IDdcbiAgICB0ZXh0OiAnSnVseSdcbiAgLFxuICAgIG51bWJlcjogOFxuICAgIHRleHQ6ICdBdWd1c3QnXG4gICxcbiAgICBudW1iZXI6IDlcbiAgICB0ZXh0OiAnU2VwdGVtYmVyJ1xuICAsXG4gICAgbnVtYmVyOiAxMFxuICAgIHRleHQ6ICdPY3RvYmVyJ1xuICAsXG4gICAgbnVtYmVyOiAxMVxuICAgIHRleHQ6ICdOb3ZlbWJlcidcbiAgLFxuICAgIG51bWJlcjogMTJcbiAgICB0ZXh0OiAnRGVjZW1iZXInXG4gIF1cbiAge1xuICAgIHJlc3RyaWN0OiAnRSdcbiAgICBzY29wZTpcbiAgICAgIHJtTW9kZWw6ICc9J1xuICAgIHRlbXBsYXRlOiBcIlwiXCI8c3BhbiBjbGFzcz1cInJtLXNlbGVjdFwiPjxzZWxlY3QgbmctbW9kZWw9XCJybU1vZGVsLnllYXJcIiBuZy1vcHRpb25zPVwieSBmb3IgeSBpbiB5ZWFyc1wiPjwvc2VsZWN0Pjwvc3Bhbj48c3BhbiBjbGFzcz1cInJtLXNlbGVjdFwiPjxzZWxlY3QgbmctbW9kZWw9XCJybU1vZGVsLm1vbnRoXCIgbmctb3B0aW9ucz1cIm4ubnVtYmVyIGFzIG4udGV4dCBkaXNhYmxlIHdoZW4gaW52YWxpZE1vbnRoKG4ubnVtYmVyKSBmb3IgbiBpbiBtb250aHNcIj48L3NlbGVjdD48L3NwYW4+XCJcIlwiXG4gICAgXG4gICAgbGluazogKHNjb3BlLCBlbCwgYXR0cnMpIC0+XG4gICAgICBlbC5hZGRDbGFzcyAncm0tbW9udGgtc2VsZWN0J1xuICAgICAgc2NvcGUucm1Nb2RlbCA9XG4gICAgICAgIG1vbnRoOiAoKG5ldyBEYXRlKS5nZXRNb250aCgpICsgMSlcbiAgICAgICAgeWVhcjogKG5ldyBEYXRlKS5nZXRZZWFyKCkgKyAxOTAwXG4gICAgICBzY29wZS5tb250aHMgPSBtb250aHNcbiAgICAgIHNjb3BlLnllYXJzID0geWVhcnNcbiAgICAgIFxuICAgICAgc2NvcGUuaW52YWxpZE1vbnRoID0gKG0pIC0+XG4gICAgICAgIGlmIHNjb3BlLnJtTW9kZWwueWVhciAhPSAobmV3IERhdGUpLmdldFllYXIoKSArIDE5MDBcbiAgICAgICAgICByZXR1cm4gZmFsc2VcbiAgICAgICAgXG4gICAgICAgIGlmIG0gPiAoKG5ldyBEYXRlKS5nZXRNb250aCgpICsgMSlcbiAgICAgICAgICByZXR1cm4gdHJ1ZVxuICB9Il19
//# sourceURL=coffeescript