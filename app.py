# -*- coding:utf-8 -*-

import json
import os
import re
import sys
import csv
import flask
import pandas as pd
from flask_csv import send_csv
from flask import Flask, request, render_template, redirect, url_for, send_file
from predict import predict_churn
from sampling import pick_sample_random, pick_sample_ordered
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import LabelBinarizer, LabelEncoder, Imputer, StandardScaler

app = Flask(__name__)

app.debug = True

global_month ="06"
global_year ="2019"
global_product = "mobile"

class DataFrameSelector(BaseEstimator, TransformerMixin):
  def __init__(self, attribute_names):
    self.attribute_names = attribute_names
  def fit(self, X, y=None):
    return self
  def transform(self, X):
    return X[self.attribute_names].values

class MyLabelBinarizer(TransformerMixin):
    def __init__(self, *args, **kwargs):
        self.encoder = LabelBinarizer(*args, **kwargs)
    def fit(self, x, y=0):
        self.encoder.fit(x)
        return self
    def transform(self, x, y=0):
        return self.encoder.transform(x)

class BaseDataTables:
    
    def __init__(self, request, columns, collection):
        
        self.columns = columns

        self.collection = collection
         
        # values specified by the datatable for filtering, sorting, paging
        self.request_values = request.values
         
 
        # results from the db
        self.result_data = None
         
        # total in the table after filtering
        self.cardinality_filtered = 0
 
        # total in the table unfiltered
        self.cadinality = 0
 
        self.run_queries()
    
    def output_result(self):
        
        output = {}

        # output['sEcho'] = str(int(self.request_values['sEcho']))
        # output['iTotalRecords'] = str(self.cardinality)
        # output['iTotalDisplayRecords'] = str(self.cardinality_filtered)
        aaData_rows = []
        
        for row in self.result_data:
            aaData_row = []
            for i in range(len(self.columns)):
                print (row, self.columns, self.columns[i])
                aaData_row.append(str(row[ self.columns[i] ]).replace('"','\\"'))
            aaData_rows.append(aaData_row)
            
        output['aaData'] = aaData_rows
        
        return output
    
    def run_queries(self):
        
         self.result_data = self.collection
         self.cardinality_filtered = len(self.result_data)
         self.cardinality = len(self.result_data)

columns = [ 'User ID', 'User Level', 'User Name', 'Churn Percentage']

@app.route('/', methods=['GET'])
def index():
    data=[{'name':'January'}, {'name':'February'}, {'name':'March'}, {'name':'April'}, {'name':'May'}, {'name':'June'}, {'name':'July'}, {'name':'August'}, {'name':'September'}, {'name':'October'}, {'name':'November'}, {'name':'December'}]
    year = [{'name':2016},{'name':2017},{'name':2018},{'name':2019}]
    
    month_to_show = month_number_to_string(global_month)
    information = 'Showing results for %s %s - %s'% (month_to_show, global_year, global_product)

    print('di sini adalah apa', file=sys.stderr)
    return render_template('index.html', columns=columns, data=data, year=year, information=information)

def month_string_to_number(string):
    m = {
        'January': '01',
        'February': '02',
        'March': '03',
        'April': '04',
         'May': '05',
         'June':'06',
         'July': '07',
         'August': '08',
         'September': '09',
         'October': '10',
         'November': '11',
         'December':'12'
        }
    s = string

    try:
        out = m[s]
        return out
    except:
        raise ValueError('Not a month')

def month_number_to_string(string):
    m = {
        '12': 'January',
        '01': 'February',
        '02': 'March',
        '03': 'April',
         '04': 'May',
         '05':'June',
         '06': 'July',
         '07': 'August',
         '08': 'September',
         '09': 'October',
         '10': 'November',
         '11':'December'
        }
    s = string

    try:
        out = m[s]
        return out
    except:
        raise ValueError('Not a month')

@app.route("/test" , methods=['GET', 'POST'])
def test():


    month = month_string_to_number(request.args["month"])
    year = request.args["year"]
    product =  request.args["product"]


    return(("%s" % (month) + " "+ year  +" "+ product )) # just to see what select is







@app.route("/predict", methods = ["GET","POST"])
def predict() :
    #Jalanin script buat query data dari DB
    #Jalanin kode buat ngepredict
    if request.method == 'GET' :
        month = month_string_to_number(request.args["month"])
        year = request.args["year"]
        product =  request.args["product"]


        global global_month
        global global_year
        global global_product
        
        global_month =month
        global_year =year
        global_product = product

        print('This is output ' + global_month + ' ' + global_year + global_product , file=sys.stderr)

    #Make file path for new json result 
    jsonpath = os.path.join(app.static_folder, 'json_files/%s_%s_%s.json' % (month, year, product))
    csv_path = os.path.join(app.static_folder, 'csv_files/%s_%s_%s.csv' % (month, year, product))

    df_result, df_result_min = predict_churn(month,year,product)    
    df_result_min.to_json(jsonpath, orient='values')
    df_result.to_csv(csv_path)


    return redirect(url_for('index'))


@app.route("/download" , methods=['GET', 'POST'])
def download():
    csv_path = os.path.join(app.static_folder, 'csv_files/%s_%s_%s.csv' % (global_month, global_year, global_product))
    
    
    return flask.send_file(csv_path,
		mimetype='text/csv',
		as_attachment=True,
		attachment_filename='%s_%s_%s.csv' % (global_month, global_year, global_product)
	)


@app.route("/test2" , methods=['GET', 'POST'])
def test2():
    option = request.args["options"]
    coupon_given = int(request.args["coupon"])

    csv_path = os.path.join(app.static_folder, 'csv_files/%s_%s_%s.csv' % (global_month, global_year, global_product))
    data = pd.read_csv(csv_path, error_bad_lines=False)
    


    if option =='randomized':
        print('random',  file=sys.stderr)
        sample = pick_sample_random(data, coupon_given)


    else:
        print('ordered',  file=sys.stderr)
        sample = pick_sample_ordered(data, coupon_given)

    print(sample , file=sys.stderr)

    csv_file = sample.to_csv("stratified_churn.csv")

    return flask.send_file(csv_file,
		mimetype='text/csv',
		as_attachment=True,
		attachment_filename='%s_%s_%s_sampling.csv' % (global_month, global_year, global_product)
	)




@app.route('/_server_data')
def get_server_data():
    jsonfile = 'json_files/%s_%s_%s.json' % (global_month, global_year, global_product)


    print('This is  output ' + jsonfile , file=sys.stderr)

    filename = os.path.join(app.static_folder, jsonfile)

    with open(filename) as blog_file:
        data = json.load(blog_file)

    results = {"aaData": data}


    return json.dumps(results)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port = 4000)
