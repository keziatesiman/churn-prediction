import os, warnings, sys
import pandas as pd, numpy as np, time
import joblib
from sklearn.preprocessing import LabelBinarizer, LabelEncoder, Imputer, StandardScaler

from sqlalchemy import create_engine
from datetime import datetime

from query import get_sales, get_user_level

def open_connection(method, host, passw, dbname):
    POSTGRES_ADDRESS = host
    if method == 'select':  
        POSTGRES_PORT = '5432'
    else:
        POSTGRES_PORT = '3001'
    POSTGRES_USERNAME = 'gardahadi-intern'
    POSTGRES_PASSWORD = passw
    POSTGRES_DBNAME = dbname
    postgres_url = ('postgresql://{username}:{password}@{ipaddress}:{port}/{dbname}'\
                    .format(username=POSTGRES_USERNAME, \
                            password=POSTGRES_PASSWORD, \
                            ipaddress=POSTGRES_ADDRESS, \
                            port=POSTGRES_PORT, \
                            dbname=POSTGRES_DBNAME))
    cnx = create_engine(postgres_url)
    return cnx


def generate_sales(month, year, product) :
    METHOD = 'select' 
    HOST = "dwh-db.c3rdq1basb0v.ap-southeast-1.rds.amazonaws.com"
    PASSWORD = "8rBqAfClCIrzHLiG62uuktiJjsplRzoP"
    DB = "dwh"
    cnx = open_connection(METHOD,HOST,PASSWORD,DB)

    df = pd.read_sql_query(get_sales(month,year,product), cnx)

    return df  

def generate_user_data() :
    METHOD = 'select' 
    HOST = 'pf-wh-analytics.c3rdq1basb0v.ap-southeast-1.rds.amazonaws.com'
    PASSWORD = "8rBqAfClCIrzHLiG62uuktiJjsplRzoP"
    DB = "pf-niffler"

    cnx = open_connection(METHOD,HOST,PASSWORD,DB)

    df = pd.read_sql_query(get_user_level(), cnx)
    return df

def generate_dataframe(month, year, product) :
    #Get dataframe from query and store in variable
    df_sales = generate_sales(month, year, product)
    df_user = generate_user_data()

    #Merge both dataframes
    df_sales = df_sales.rename(columns={'user_id': 'userId'})
    df_merged = pd.merge(left=df_sales,right=df_user, left_on='userId', right_on='userId')
    df_merged = df_merged.drop_duplicates()
    df_fullName = pd.DataFrame( data= {'fullName' : df_merged["fullName"]})
    df_merged = df_merged.drop(["fullName"], axis=1)
    
    return df_fullName, df_merged    


#PR : Sambungin sama user level
#Sementara Manual dlu 



